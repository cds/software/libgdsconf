/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: conftype						*/
/*                                                         		*/
/* Module Description: Types for the configuration information API	*/
/*									*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDSCOND_CONFTYPE_H
#define _GDSCOND_CONFTYPE_H

#ifdef __cplusplus
extern "C" {
#endif

#include <unistd.h>

/*@{*/

/** Service name of arbitrary waveform generator */
#define CONFIG_SERVICE_AWG "awg"

/** Service name of test point manager */
#define CONFIG_SERVICE_TP "tp"

/** Service name of network data server */
#define CONFIG_SERVICE_NDS "nds"

/** Service name of diagnostics test kernel */
#define CONFIG_SERVICE_TEST "tst"

/** Service name of error logger */
#define CONFIG_SERVICE_ERR "err"

/** Service name of channel information database */
#define CONFIG_SERVICE_CHN "chn"

/** Service name of channel information database */
#define CONFIG_SERVICE_LEAP "leap"

/** Service name of network time server */
#define CONFIG_SERVICE_NTP "ntp"

/** Service name of calibration server */
#define CONFIG_SERVICE_CAL "cal"

/** Service name of launch server */
#define CONFIG_SERVICE_LAUNCH "launch"

/** Configuration information record.
************************************************************************/
typedef struct gdsconf_confinfo_t
{
    /** interface name */
    char interface[ 8 ];
    /** interferometer number (-1 for *) */
    int node;
    /** interface id number (-1 for *) */
    int num;
    /** host name */
    char host[ 64 ];
    /** port/program number of interface (-1 for *) */
    int port_prognum;
    /** interface version (-1 for *) */
    int progver;
} gdsconf_confinfo_t;

/**
 * Record used by clients that includes a gdsconf_confinfo_t and a sender address
 *
 */
typedef struct gdsconf_record_t
{
    /** interface name */
    char interface[ 8 ];
    /** interferometer number (-1 for *) */
    int node;
    /** interface id number (-1 for *) */
    int num;
    /** host name */
    char host[ 64 ];
    /** port/program number of interface (-1 for *) */
    int port_prognum;
    /** interface version (-1 for *) */
    int progver;

    // an internet address to the server that sent the conf_info.  Typically
    // this is a nnn.nnn.nnn.nnn IP address as a string.
    char sender_address[ 64 ];
} gdsconf_record_t;

/**
 * Convert a gdsconf_record_t structure to a standard configuration string.
 *
 * <interface> <node> <num> <host> <port_prognum> <progver> <sender_address>
 *
 * Where node, num, port_prognum, progver can be integers are '*', with '*' == -1 in the struct.
 *
 * @param record the record to convert
 * @param buffer A character buffer to hold the string
 * @param buffer_size size of the buffer
 * @returns 1 if successful, 0 otherwise, probably a memory allocation error.
 */
int gdsconf_record_to_string( gdsconf_record_t* record,
                              char*             buffer,
                              size_t            buffer_size );

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_GDSCONF_CONFTYPE_H */
