/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: confserver						*/
/*                                                         		*/
/* Module Description: Configuration information server			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29July99 D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: confservers.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_CONFSERVER_H
#define _GDS_CONFSERVER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Header File List: */
#include "gdsconfcommon.h"
#include "gdsconftype.h"

typedef void* GDSCONF_SERVER_HANDLE;

/** @name Configuration Server API
    This API provides an implementation of a configuration information
    server.

    @memo Configuration Server API
    @author Written July 1999 by Daniel Sigg
    @version 0.1
************************************************************************/

/*@{*/

/** Starts a configuration information server. It requires a list
    of confinfo_t structures as an argument.  Each element describes a different service.

    If successful this function will start a thread which listens at the configuration
    server port for requests, and return a handle to the server.

    The code should call gdsconf_server_stop() to shutdown the thread and destroy the handle.

    @param confs list of confinfo_t structs, one element defines one service
    @param num number of elements in confs
    @return NULL on failure, otherwise a handle to the server.
************************************************************************/
GDSCONF_SERVER_HANDLE gdsconf_server_start( const gdsconf_confinfo_t confs[],
                                            int num_confs );

/**
 * Orders the configuraiton server to shut down, joins the thread, and destroys the handle.
 * conf_server is not usale after this function is called.
 */
void gdsconf_server_stop( GDSCONF_SERVER_HANDLE conf_server );

/**
 * Helper function that can be used to fill in standard defaults for a gdsconf_confinfo structure
 *
 * id is set to zero and host is set to a likely IP address
 *
 * @param conf_info must be pre-allocated.
 */
void gdsconf_confinfo_setup( struct gdsconf_confinfo_t* conf_info,
                             const char*                interface,
                             int                        node,
                             int                        prognum,
                             int                        progver );

/*@}*/

#ifdef __cplusplus
}
#endif

#endif /*_GDS_CONFSERVER_H */
