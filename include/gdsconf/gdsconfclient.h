//
// Created by erik.vonreis on 8/7/23.
//

#ifndef LIBGDSCONF_GDSCONFCLIENT_H
#define LIBGDSCONF_GDSCONFCLIENT_H

#include "gdsconftype.h"
#include "gdsconfcommon.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Handle to a bundle of configurations as discovered on the network.
 */
typedef void* GDSCONF_HANDLE;

/**
 * Gather configuration records.  These records can be retrieved with gdsconf_get_confinfo().
 *
 * These functions are thread safe.  The handle is valid until gdsconf_free_handle() is called on the handle.
 *
 * @param timeout_s time to wait for responses from configuration servers.  Since responses can come from
 * any number of servers, the function always waits the full timeout period UNLESS environment variable LIGO_INJ_MODEL
 * is set to a comma-delimited list of models, in which case the function reads those models from a locally mounted
 * testpoint.par file as fast as it can.
 * @return a handle to a new bundle of configurations.  NULL otherwise or errno is set.
 */
GDSCONF_HANDLE gdsconf_refresh_records( int timeout_s );

/**
 * Return a handle to the latest set of configuration records.  If no records have yet been retrieved, then
 * gdsconf_refresh_records is called with a sensible timeout.
 *
 * These functions are thread safe.  The handle is valid until gdsconf_free_handle() is called on the handle.
 *
 * @return a handle to a new bundle of configurations.  NULL otherwise or errno is set.
 */
GDSCONF_HANDLE gdsconf_get_records( );

/**
 * Return configuration records associated with a handle.  The records pointer is valid until gdsconf_free_handle() is
 * called on the handle.
 *
 * These functions are thread safe.
 *
 * @param records
 * @param num_records
 * @return true if successful.  If failed, errno is set.
 */
int gdsconf_get_confinfo( GDSCONF_HANDLE     handle,
                          gdsconf_record_t** records,
                          size_t*            num_records );

/**
 * Free the memory used by the configuration handle.
 *
 * Call after
 * @param handle
 */
void gdsconf_free_handle( GDSCONF_HANDLE handle );

/**
 * Determine if the gdsconf client supports a given network interface version.
 * @param interface_version
 * @return non-zero if the given version is supported, zero otherwise.
 */
int gdsconf_client_supports_network_interface_version( int interface_version );

#ifdef __cplusplus
}
#endif

#endif //LIBGDSCONF_GDSCONFCLIENT_H
