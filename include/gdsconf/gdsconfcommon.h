//
// Created by erik.vonreis on 9/13/23.
//

#ifndef LIBGDSCONF_GDSCONFCOMMON_H
#define LIBGDSCONF_GDSCONFCOMMON_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Get the version of the library, which is
 * major_version.minor_version.release_version
 *
 * @param major_version
 * @param minor_version
 * @param release_version
 */
void
gdsconf_version( int* major_version, int* minor_version, int* release_version );

/**
 * Check that the library is compatible with major_version.minor_version.release_version
 *
 * 'compatible' means the given version is <= the library version, and the given version is ABI compatible.
 *
 * If the version is compatible, "NULL" is returned.
 *
 * If not compatible, a string is returned with details.
 * The string is owned by the library and should not be freed.
 *
 *
 * @param major_version
 * @param minor_version
 * @param release_version
 * @return NULL if compatible.  An explanation string otherwise.
 */
const char* gdsconf_is_compatible_with( int major_version,
                                        int minor_version,
                                        int release_version );

/**
 * Report the preferred network interface version.
 * @return
 */
int gdsconf_network_interface_version( );

#ifdef __cplusplus
}
#endif

#endif //LIBGDSCONF_GDSCONFCOMMON_H
