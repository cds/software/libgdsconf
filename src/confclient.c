/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: confinfo						*/
/*                                                         		*/
/* Module Description: implements functions for controlling the AWG	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE 1
#endif

/*#ifndef DEBUG
#define DEBUG
#endif*/

/* Header File List: */
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <stdio.h>
#include <sys/types.h>
#include <netdb.h> /* For gethostbyname() */
#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/time.h>
#include <errno.h>

#include "conf_common.h"
#include "gdsconf/gdsconfclient.h"
#include "confclient_internal.h"
#include "ndshost.h"
#include "readtppar.h"
#include "rpcdefs.h"

#define DEFAULT_TIMEOUT_S 2
#define START_SET_SIZE 16

pthread_mutex_t conf_set_mutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * holds the set of all configurations found when searching the network
 */
typedef struct conf_set
{
    gdsconf_record_t* records;
    size_t            size;
    size_t            count;
    int               references;
} conf_set;

int
gdsconf_get_confinfo( GDSCONF_HANDLE     confs_handle,
                      gdsconf_record_t** records,
                      size_t*            num_records )
{

    if ( NULL == confs_handle )
    {
        errno = EINVAL;
        return 0;
    }

    conf_set* set = (conf_set*)confs_handle;

    *records = set->records;
    *num_records = set->count;
    return 1;
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: sortConfs					*/
/*                                                         		*/
/* Procedure Description: compares 2 configuration list entries		*/
/*                                                         		*/
/* Procedure Arguments: entry 1, entry 2				*/
/*                                                         		*/
/* Procedure Returns: 0 if equal, <0 if smaller, >0 if larger		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static int
compareConfs( const gdsconf_record_t* c1, const gdsconf_record_t* c2 )
{
    int cmp = strcasecmp( c1->interface, c2->interface );
    if ( cmp != 0 )
    {
        return cmp;
    }

    if ( c1->node < c2->node )
    {
        return -1;
    }
    if ( c1->node > c2->node )
    {
        return 1;
    }

    if ( c1->num < c2->num )
    {
        return -1;
    }
    if ( c1->num > c2->num )
    {
        return 1;
    }

    cmp = strcasecmp( c1->host, c2->host );
    if ( cmp != 0 )
    {
        return cmp;
    }

    if ( c1->port_prognum < c2->port_prognum )
    {
        return -1;
    }
    if ( c1->port_prognum > c2->port_prognum )
    {
        return 1;
    }

    if ( c1->progver < c2->progver )
    {
        return -1;
    }
    if ( c1->progver > c2->progver )
    {
        return 1;
    }

    cmp = strcasecmp( c1->sender_address, c2->sender_address );
    if ( cmp != 0 )
    {
        return cmp;
    }

    return 0;
}

/**
 * Atomically store the latest configuration set
 */
conf_set* latest_conf_set = NULL;

/**
 * Create an empty set with zero references
 * @return
 */

static void
grow_conf_set( conf_set* set, size_t new_size )
{
    if ( new_size > set->size )
    {
        gdsconf_record_t* new_array = (gdsconf_record_t*)reallocarray(
            set->records, sizeof( gdsconf_record_t ), new_size );
        if ( NULL != new_array )
        {
            set->records = new_array;
            set->size = new_size;
        }
        else
        {
            fprintf(
                stderr,
                "ERR out of memory: failure to resize configuration set\n" );
        }
    }
}

/**
 * Free memory associated with a conf_set
 * @param set
 */
static void
free_conf_set( conf_set* set )
{
    if ( NULL != set )
    {
        if ( NULL != set->records )
        {
            free( set->records );
        }
        free( set );
    }
}

static conf_set*
create_conf_set( )
{
    conf_set* new_set = (conf_set*)malloc( sizeof( conf_set ) );
    if ( NULL == new_set )
    {
        return NULL;
    }

    memset( new_set, 0, sizeof( conf_set ) );

    grow_conf_set( new_set, START_SET_SIZE );
    if ( NULL == new_set->records )
    {
        free_conf_set( new_set );
        return NULL;
    }

    return new_set;
}

/**
 *
 * Add a record to the set, growing if needed.
 *
 * Does nothing but returns success if new_record is NULL.
 *
 * @param set
 * @param new_record
 * @return 1 if successful.  0 if out of memory.
 */
static int
add_to_conf_set( conf_set* set, gdsconf_record_t* new_record )
{
    if ( NULL == new_record )
    {
        return 1;
    }

    size_t low = 0;
    size_t high = set->count;

    // find insertion point to keep the list sorted
    int cmp;
    while ( high - low > 1 )
    {
        size_t mid = ( high + low ) / 2;
        cmp = compareConfs( new_record, set->records + mid );
        if ( cmp < 0 )
        {
            high = mid;
        }
        else
        {
            low = mid;
        }
    }

    int new_index = 0;
    if ( set->count > 0 )
    {
        cmp = compareConfs( new_record, set->records + low );
        if ( 0 == cmp )
        {
            // duplicate, don't add
            return 1;
        }

        new_index = ( cmp < 0 ) ? low : ( low + 1 );
    }

    if ( set->size <= set->count )
    {
        grow_conf_set( set, set->size * 2 );
        if ( set->size <= set->count )
        {
            // didn't grow
            errno = ENOMEM;
            return 0;
        }
    }

    // shift later records up one element.
    int num2copy = set->count - new_index;
    if ( num2copy > 0 )
    {
        memmove( set->records + new_index + 1,
                 set->records + new_index,
                 num2copy * sizeof( set->records[ 0 ] ) );
    }

    // insert new element
    memcpy( set->records + new_index, new_record, sizeof( gdsconf_record_t ) );

    set->count++;

    return 1;
}

static void
ref_confset( conf_set* set )
{
    ++( set->references );
}

static void
deref_confset( conf_set* set )
{
    --( set->references );
    if ( set->references == 0 )
    {
        // no more references
        free_conf_set( set );
    }
}

static void
set_confset_as_latest( conf_set* set )
{
    conf_set* last;
    pthread_mutex_lock( &conf_set_mutex );
    ref_confset( set );
    last = latest_conf_set;
    latest_conf_set = set;
    if ( NULL != last )
    {
        deref_confset( last );
    }
    pthread_mutex_unlock( &conf_set_mutex );
}

static conf_set*
get_latest_records( )
{
    pthread_mutex_lock( &conf_set_mutex );
    conf_set* conf_set = latest_conf_set;
    if ( NULL != conf_set )
    {
        ++( conf_set->references );
    }
    pthread_mutex_unlock( &conf_set_mutex );
    return conf_set;
}

#define IP_ADDR_LEN 64

/**
 * Add nds to conf_set
 * @param set
 * @return 1 if successful, otherwise 0 and errno is set.
 *   EINVAL if either some argumnens are NULL OR the environment variables are badly configured.
 */
static int
add_nds_to_confset( conf_set* set )
{
    char ndshost[ 2 ][ 256 ];
    int  port[ 2 ];
    int  retval;

    retval = getNDSHostPort( ndshost[ 0 ], port, ndshost[ 1 ], port + 1 );
    if ( retval < 0 || retval >= 5 )
    {
        // error
        errno = EINVAL;
        return 0;
    }

    gdsconf_record_t record;

    for ( int i = 0; i < 2; ++i )
    {

        strcpy( record.interface, "nds" );
        char ip_addr[ IP_ADDR_LEN ];
        getHostAddress( *( ndshost + i ), ip_addr, IP_ADDR_LEN );
        strncpy( record.host, ip_addr, sizeof( record.host ) );
        record.node = -1;
        record.num = -1;
        record.port_prognum = *( port + i );
        record.progver = -1;
        strcpy( record.sender_address, "127.0.0.1" );

        if ( !add_to_conf_set( set, &record ) )
        {
            return 0;
        }
    }

    return 1;
}

/**
 * Add awg entries from locally read par files
 * @param set
 * @param model_list_str a comma delimited string of model names
 * @return 1 if good, zero otherwise
 */
int
add_local_services( conf_set* set, char* model_list_str )
{
    modelInfo* node = NULL;
    int        nread = 0;

    gdsconf_record_t record;

    /* Get the list of awg and tp servers from the LIGO_INJ_MODEL env. variable value. */
    /* Look in the testpoint.par file, found in $CDSROOT/target/gds/param/testpoint.par */
    if ( readTestpointPar( &node, &nread ) == 0 )
    {

        //        fprintf( stderr, "Number read is %d\n", nread );
        //        for ( i = 0; i < nread; i++ )
        //        {
        //            fprintf( stderr,
        //                     "%d: dcuid= %d, system= %s\n",
        //                     i,
        //                     node[ i ].dcuid,
        //                     node[ i ].modelname );
        //        }

        /* env_val should contain 1 or more model names separated by commas.  For each name,
     * find the corresponding dcuid and hostname in the nodes read from the testpoint.par file
     * and construct a string for the RPC address for the awg and testpoint managers.
     */
        char* saveptr;
        char* pch = strtok_r( model_list_str, ",", &saveptr );
        while ( pch != (char*)NULL )
        {
            // fprintf( stderr, "Locating system for model %s\n", pch );

            /* Find the model name in the nodes found in the testpoint.par file. */
            for ( int i = 0; i < nread; i++ )
            {
                if ( strcmp( node[ i ].modelname, pch ) == 0 )
                {
                    /* Found the node. */
                    struct hostent* host;
                    char            ipstr[ INET_ADDRSTRLEN ];

                    /* We have a host name, but we want the IP address. */
                    host = gethostbyname( node[ i ].hostname );
                    if ( !host )
                        break;
                    (void)inet_ntop( host->h_addrtype,
                                     *( host->h_addr_list ),
                                     ipstr,
                                     sizeof( ipstr ) );

                    strcpy( record.interface, "awg" );
                    record.node = node[ i ].dcuid;
                    record.num = 0;
                    strncpy( record.host, ipstr, sizeof( record.host ) );
                    record.port_prognum = RPC_PROGNUM_AWG + node[ i ].dcuid;
                    record.progver = 1;
                    strcpy( record.sender_address, "127.0.0.1" );

                    add_to_conf_set( set, &record );

                    strcpy( record.interface, "tp" );
                    record.node = node[ i ].dcuid;
                    record.num = 0;
                    strncpy( record.host, ipstr, sizeof( record.host ) );
                    record.port_prognum =
                        RPC_PROGNUM_TESTPOINT + node[ i ].dcuid;
                    record.progver = 1;
                    strcpy( record.sender_address, "127.0.0.1" );

                    add_to_conf_set( set, &record );
                    break;
                }
            }
            /* There may be another... */
            pch = strtok_r( NULL, ",", &saveptr );
        }
    }

    // got malloced pointer from readTestpointPar()
    if ( NULL != node )
    {
        free( node );
    }
    return 1;
}

/**
 * Populate a configuration set with services learned through polling broadcast networks
 * @param set The set to populate
 * @param addresses The broadcast addresses to try.
 * @param address_count the size of the addresses array
 * @return 1 if successful.  0 otherwise.
 */
int
add_broadcast_services( conf_set*                 set,
                        const struct sockaddr_in* addresses,
                        size_t                    address_count,
                        int                       timeout_s )
{

    ssize_t nbytes;
    char    out_buf[ 4 ]; /* buffer */
    char    in_buf[ 1024 ];

    /* Get the list of awg and tp servers by broadcasting for them. */
    int sock = -1;

    /* create a socket */
    sock = socket( PF_INET, SOCK_DGRAM, 0 );
    if ( sock == -1 )
    {
        return 0;
    }
    /* enable broadcast */
    int nset = 1;
    if ( setsockopt(
             sock, SOL_SOCKET, SO_BROADCAST, (char*)&nset, sizeof( nset ) ) ==
         -1 )
    {
        close( sock );
        return 0;
    }

    struct sockaddr_in bind_addr;
    /* bind socket */
    bind_addr.sin_family = AF_INET;
    bind_addr.sin_port = 0;
    bind_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    if ( bind( sock, (struct sockaddr*)&bind_addr, sizeof( bind_addr ) ) )
    {
        close( sock );
        return 0;
    }

    *( (long*)out_buf ) = htonl( 0 );

    int                diff;
    fd_set             readfds;
    struct timeval     wait;
    struct sockaddr_in recv_addr;
    char               sender[ 256 ];
    gdsconf_record_t   record;

    char* savestr = NULL;
    char* single_service_str = NULL;

    for ( int i = 0; i < address_count; ++i )
    {
        nbytes = sendto( sock,
                         out_buf,
                         4,
                         0,
                         (struct sockaddr*)addresses + i,
                         sizeof( struct sockaddr_in ) );
        if ( nbytes < 0 )
        {
            close( sock );
            return 0;
        }

        /* wait for answers */
        time_t start = time( NULL );

        int real_timeout_s = DEFAULT_TIMEOUT_S;
        if ( timeout_s > 0 )
        {
            real_timeout_s = timeout_s;
        }

        while ( ( diff = ( time( NULL ) - start ) ) <= real_timeout_s )
        {
            /* poll socket */
            FD_ZERO( &readfds );
            FD_SET( sock, &readfds );
            wait.tv_sec = diff;
            wait.tv_usec = 0;
            nset = select( FD_SETSIZE, &readfds, 0, 0, &wait );
            if ( nset < 0 )
            {
                close( sock );
                return 0;
            }
            else if ( nset == 0 )
            {
                continue;
            }
            /* get answer */
            socklen_t size = sizeof( struct sockaddr_in );
            nbytes = recvfrom( sock,
                               in_buf,
                               sizeof( in_buf ) - 1,
                               0,
                               (struct sockaddr*)&recv_addr,
                               &size );
            inet_ntop(
                AF_INET, &( recv_addr.sin_addr ), sender, sizeof( sender ) );
            in_buf[ nbytes ] = 0;
            char* tok_buf = in_buf + 4;
            while ( ( single_service_str =
                          strtok_r( tok_buf, "\n", &savestr ) ) != NULL )
            {
                tok_buf = NULL;
                if ( parseConfInfo( single_service_str, &record ) )
                {
                    // do nothing with a bad packet
                    fprintf( stderr,
                             "Got an invalid service: %s\n",
                             single_service_str );
                }
                else
                {
                    inet_ntop( AF_INET,
                               &recv_addr.sin_addr,
                               record.sender_address,
                               sizeof( record.sender_address ) );
                    add_to_conf_set( set, &record );
                }
            }
        }
    }
    close( sock );
    return 1;
}

/**
 * Return a list of addresses based on a string list of dot-notation IP addresses, comma separated.
 * @param count output: the number of elements in the list.
 * @param list_str input: the comma separated string of ip addresses.  Each address turns into a single elment of the returned list.
 * @return the list, or NULL on allocation failure.  Returned list is malloced and should be freed by the caller.
 */
static struct sockaddr_in*
get_addresses_from_list( size_t* count, const char* list_str )
{
    char   working_buffer[ 1024 ];
    size_t size = 8;
    *count = 0;

    struct sockaddr_in* list =
        (struct sockaddr_in*)malloc( sizeof( struct sockaddr_in ) * size );
    if ( NULL == list )
    {
        return NULL;
    }

    strncpy( working_buffer, list_str, sizeof( working_buffer ) );

    char* savestr;

    char* pch = strtok_r( working_buffer, ",", &savestr );
    while ( pch != (char*)NULL )
    {
        if ( *count >= size )
        {
            size_t              new_size = size * 2;
            struct sockaddr_in* new_list = (struct sockaddr_in*)realloc(
                new_list, sizeof( struct sockaddr_in ) * new_size );
            if ( NULL == new_list )
            {
                // return as much as we can get
                return list;
            }
            list = new_list;
            size = new_size;
        }

        // fprintf(stderr, "Locating system for model %s\n", pch);
        if ( inet_pton( AF_INET, pch, &list[ *count ].sin_addr ) != 0 )
        {
            list[ *count ].sin_family = AF_INET;
            list[ *count ].sin_port = ntohs( _MAGICPORT );
        }
        *count += 1;

        /* Find the model name in the nodes found in the testpoint.par file. */
        /* There may be another... */
        pch = strtok_r( NULL, ",", &savestr );
    }
    return list;
}

/**
 * Return a list of broadcast addresses in network byte order
 *
 * @param count output: number of elements in the returned list.
 * @return NULL on error.  If not NULL, free the list when done.
 */
static struct sockaddr_in*
get_broadcast_addresses( size_t* count )
{
    char*               env_val;
    struct sockaddr_in* list;
    if ( ( env_val = getenv( "LIGO_RT_BCAST" ) ) != (char*)NULL )
    {
        return get_addresses_from_list( count, env_val );
    }
    else
    {
        list = (struct sockaddr_in*)malloc( sizeof( struct sockaddr_in ) );
        if ( NULL == list )
        {
            return NULL;
        }
        list[ 0 ].sin_family = AF_INET;
        list[ 0 ].sin_addr.s_addr = htonl( INADDR_BROADCAST );
        list[ 0 ].sin_port = (short)htons( _MAGICPORT );
        *count = 1;
        return list;
    }
}

GDSCONF_HANDLE
gdsconf_get_records( )
{
    GDSCONF_HANDLE conf = get_latest_records( );
    if ( NULL != conf )
    {
        return conf;
    }
    return gdsconf_refresh_records( DEFAULT_TIMEOUT_S );
}

GDSCONF_HANDLE
gdsconf_refresh_records( int timeout_s )
{
    conf_set* new_set = create_conf_set( );
    if ( !add_nds_to_confset( new_set ) )
    {
        return NULL;
    }

    char* env_val;

    if ( ( env_val = getenv( "LIGO_INJ_MODEL" ) ) != (char*)NULL )
    {
        if ( !add_local_services( new_set, env_val ) )
        {
            // do nothing
            fprintf(
                stderr,
                "failed to add services using local testpoint.par file.\n" );
        }
    }
    else
    {
        size_t              address_count;
        struct sockaddr_in* bcast_adds =
            get_broadcast_addresses( &address_count );
        if ( NULL == bcast_adds )
        {
            fprintf( stderr,
                     "attempted to find services by broadcast but could not "
                     "find broadcast addresses.\n" );
            // do nothing
        }
        if ( !add_broadcast_services(
                 new_set, bcast_adds, address_count, timeout_s ) )
        {
            // do nothing
            fprintf( stderr,
                     "failure while waiting for responses from configuraiton "
                     "servers.\n" );
        }
        free( bcast_adds );
    }

    set_confset_as_latest( new_set );

    return get_latest_records( );
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: parseConfInfo				*/
/*                                                         		*/
/* Procedure Description: parses a conf. info string			*/
/*                                                         		*/
/* Procedure Arguments: info string, info record (return)		*/
/*                                                         		*/
/* Procedure Returns: 0 if successful, <0 otherwise			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
int
parseConfInfo( const char* info, gdsconf_record_t* rec )
{
    char* p; /* token */
    char  buf[ 1024 ]; /* buffer */
    char* wrk; /* work space */

    strncpy( buf, info, sizeof( buf ) );
    buf[ sizeof( buf ) - 1 ] = 0;

    /* get interface */
    p = strtok_r( buf, " \t\n", &wrk );
    if ( p == NULL )
    {
        return -1;
    }
    strncpy( rec->interface, p, sizeof( rec->interface ) );
    rec->interface[ sizeof( rec->interface ) - 1 ] = 0;

    /* get interferometer number */
    p = strtok_r( NULL, " \t\n", &wrk );
    if ( p == NULL )
    {
        return -2;
    }
    if ( ( strlen( p ) > 0 ) && ( p[ 0 ] == '*' ) )
    {
        rec->node = -1;
    }
    else
    {
        rec->node = atoi( p );
    }

    /* get id number */
    p = strtok_r( NULL, " \t\n", &wrk );
    if ( p == NULL )
    {
        return -3;
    }
    if ( ( strlen( p ) > 0 ) && ( p[ 0 ] == '*' ) )
    {
        rec->num = -1;
    }
    else
    {
        rec->num = atoi( p );
    }

    /* get host name */
    p = strtok_r( NULL, " \t\n", &wrk );
    if ( p == NULL )
    {
        return -4;
    }
    strncpy( rec->host, p, sizeof( rec->host ) );
    rec->host[ sizeof( rec->host ) - 1 ] = 0;

    /* get port/prognum */
    p = strtok_r( NULL, " \t\n", &wrk );
    if ( p == NULL )
    {
        return -5;
    }
    if ( ( strlen( p ) > 0 ) && ( p[ 0 ] == '*' ) )
    {
        rec->port_prognum = -1;
    }
    else
    {
        rec->port_prognum = atoi( p );
    }

    /* get program version */
    p = strtok_r( NULL, " \t\n", &wrk );
    if ( p == NULL )
    {
        return -6;
    }
    if ( ( strlen( p ) > 0 ) && ( p[ 0 ] == '*' ) )
    {
        rec->progver = -1;
    }
    else
    {
        rec->progver = atoi( p );
    }

    return 0;
}

void
gdsconf_free_handle( GDSCONF_HANDLE handle )
{
    conf_set* set = (conf_set*)handle;
    pthread_mutex_lock( &conf_set_mutex );
    deref_confset( set );
    pthread_mutex_unlock( &conf_set_mutex );
}

int
gdsconf_client_supports_network_interface_version( int interface_version )
{
    return interface_version == 1;
}