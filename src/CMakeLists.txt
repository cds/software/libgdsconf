set(CMAKE_SHARED_LINKER_FLAGS "-Wl,--no-undefined")

add_library(gdsconf SHARED
        confserver.c
        confclient.c
        conftype.c
        ndshost.c
        readtppar.c
        confcommon.c
)

target_link_libraries(gdsconf
    pthread
)

target_include_directories(gdsconf PUBLIC
    ${CMAKE_SOURCE_DIR}/include
)

set_target_properties(gdsconf
    PROPERTIES
        VERSION 0.1.0
        PUBLIC_HEADER
            "../include/gdsconf/gdsconfclient.h;../include/gdsconf/gdsconfserver.h;../include/gdsconf/gdsconftype.h;../include/gdsconf/gdsconfcommon.h"
)

install(TARGETS gdsconf
        LIBRARY
            DESTINATION lib
            COMPONENT Libraries
            NAMELINK_COMPONENT Development
        PUBLIC_HEADER
            DESTINATION include/gdsconf
            COMPONENT Development
)