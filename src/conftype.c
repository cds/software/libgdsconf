//
// Created by erik.vonreis on 9/11/23.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "gdsconf/gdsconftype.h"

/**
 * Convert a value to a string that represents the number but is a '*' if value == -1
 * @param buff a pre-allocated output buffer
 * @param n  size of buff
 * @param value the value to convert
 * @return returns buff
 */
static char*
convert_to_star( char* buff, int n, long value )
{
    if ( value == -1 )
    {
        strncpy( buff, " *", n );
    }
    else
    {
        snprintf( buff, n, " %ld", value );
    }
    return buff;
}

/**
 * Convert a confinfo_t structure into a gdsconf compatible string
 *
 * String format is "<interface> <node> <num> <host> <prognum> <progver>"
 * <interface> is a short string naming the type of service.  It should match a string defined in conftypes.h
 * <node> an integer node number or * for any node.  For front ends, should match DCUID
 * <num> an integer number or * for any num - always zero in known uses
 * <hostname> string ip address for the server host.  This is sometimes wrong so should always be ignored.
 * <prognum> rpc program number
 * <progver> rpc program version
 */
#define SERVICE_STRING_SIZE 256
char*
conf_info_to_string( const gdsconf_confinfo_t* conf_info )
{
    char* service_string = malloc( SERVICE_STRING_SIZE );
    if ( NULL == service_string )
    {
        return NULL;
    }
    char buff[ 128 ];
    snprintf(
        service_string, SERVICE_STRING_SIZE - 1, "%s", conf_info->interface );
    strncat( service_string,
             convert_to_star( buff, sizeof( buff ) - 1, conf_info->node ),
             SERVICE_STRING_SIZE - 1 );
    strncat( service_string,
             convert_to_star( buff, sizeof( buff ) - 1, conf_info->num ),
             SERVICE_STRING_SIZE - 1 );
    strncat( service_string, " ", SERVICE_STRING_SIZE - 1 );
    strncat( service_string, conf_info->host, SERVICE_STRING_SIZE - 1 );
    strncat(
        service_string,
        convert_to_star( buff, sizeof( buff ) - 1, conf_info->port_prognum ),
        SERVICE_STRING_SIZE - 1 );
    strncat( service_string,
             convert_to_star( buff, sizeof( buff ) - 1, conf_info->progver ),
             SERVICE_STRING_SIZE - 1 );
    return service_string;
}

int
gdsconf_record_to_string( gdsconf_record_t* record,
                          char*             buffer,
                          size_t            buffer_size )
{
    char* cinfo_str = conf_info_to_string( (gdsconf_confinfo_t*)record );
    if ( NULL != cinfo_str )
    {
        snprintf(
            buffer, buffer_size, "%s %s", cinfo_str, record->sender_address );
        free( cinfo_str );
        return 1;
    }
    return 0;
}