/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: confserver						*/
/*                                                         		*/
/* Module Description: implements functions for configuration server	*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef DEBUG
#define DEBUG
#endif

/* Header File List: */
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include "conf_common.h"
#include "gdsconf/gdsconfserver.h"
#include "confserver_internal.h"
#include "conftype_internal.h"
#include "pthread.h"

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: sendAnswer					*/
/*                                                         		*/
/* Procedure Description: answer service requests			*/
/*                                                         		*/
/* Procedure Arguments: id, sender address, answer string		*/
/*                                                         		*/
/* Procedure Returns: void						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void
sendAnswer( u_int                    id,
            struct sockaddr_in*      name,
            const char*              answer,
            gdsconf_server_instance* server )
{

    ssize_t nbytes; /* # of bytes send */
    size_t  size; /* size of message */
    size_t  len; /* length of send block */
    char    message[ 2048 ]; /* send message */

    *( (long*)message ) = htonl( id );
    size = strlen( answer );
    len = 0;

    while ( len < size )
    {
        nbytes = size - len;
        if ( nbytes <= 1024 )
        {
            memcpy( message + 4, answer + len, nbytes );
        }
        else
        {
            /* break answers at space */
            nbytes = 1024;
            memcpy( message + 4, answer + len, nbytes );
            while ( ( nbytes > 0 ) && ( message[ 3 + nbytes ] != ' ' ) )
            {
                nbytes--;
            }
            if ( nbytes == 0 )
            {
                /*close (sock);*/
                return;
            }
        }
        //printf("Sending '%s' in reply\n", message);
        nbytes += 4;
        ssize_t nsent;
        nanosleep( &server->delay_time, (struct timespec*)NULL );
        for ( ;; )
        {
            nsent = sendto( server->sock,
                            message,
                            nbytes,
                            0,
                            (struct sockaddr*)name,
                            sizeof( struct sockaddr_in ) );
            if ( nsent < 0 )
                perror( "sendto" );
            if ( nsent == nbytes )
                break;
        }
        len += nbytes;
    }
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Internal Procedure Name: waitForRequests				*/
/*                                                         		*/
/* Procedure Description: waits for service requests			*/
/*                                                         		*/
/* Procedure Arguments: void						*/
/*                                                         		*/
/* Procedure Returns: never						*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
static void
waitForRequests( struct gdsconf_server_instance* server )
{
    struct sockaddr_in name; /* sender address */
    socklen_t          size; /* size of address */
    char               message[ 2049 ]; /* recv datagram */
    ssize_t            nbytes; /* # of recv. bytes */
    uint               id; /* request id */
    int                i; /* service index */

    while ( !server->closing )
    {
        /* Wait for a datagram.  */
        size = sizeof( name );
        nbytes = recvfrom(
            server->sock, message, 2048, 0, (struct sockaddr*)&name, &size );

        /* error */
        if ( nbytes < 0 )
        {
            if ( errno == EINTR || errno == EAGAIN || errno == EWOULDBLOCK )
            {
                continue;
            }
            else
            {
                return;
            }
        }
        /* received a request */
        else if ( nbytes >= 4 )
        {
            // request id is now unused value.  Should always be zero.
            id = ntohl( *( (long*)message ) );
            if ( 0 != id )
            {
                fprintf( stderr,
                         "Expected zero for gdsconf request id, but got %d\n",
                         id );
            }
            message[ nbytes ] = 0;
            /* answer */
            for ( i = 0; i < server->num_conf_infos; ++i )
            {
                sendAnswer( id, &name, server->service_strings[ i ], server );
            }
        }
    }
}

void*
conf_server_thread_loop( void* server_v )
{
    gdsconf_server_instance* server = server_v;
    waitForRequests( server );
    close( server->sock );
    return NULL;
}

int
open_socket( gdsconf_server_instance* server )
{
    /* create new server socket */
    server->sock = socket( PF_INET, SOCK_DGRAM, 0 );
    if ( server->sock == -1 )
    {
        return -2;
    }

    {
        int nset = 1;
        if ( setsockopt( server->sock,
                         SOL_SOCKET,
                         SO_REUSEADDR,
                         (char*)&nset,
                         sizeof( nset ) ) == -1 )
        {
            close( server->sock );
            return -3;
        }
        struct timeval recv_timeout = { 1, 0 };
        if ( setsockopt( server->sock,
                         SOL_SOCKET,
                         SO_RCVTIMEO,
                         &recv_timeout,
                         sizeof( struct timeval ) ) == -1 )
        {
            return -5;
        }
    }

    struct sockaddr_in name;

    /* connect socket to IP/port */
    name.sin_family = AF_INET;
    name.sin_port = htons( _MAGICPORT );
    name.sin_addr.s_addr = htonl( INADDR_ANY );
    if ( bind( server->sock, (struct sockaddr*)&name, sizeof( name ) ) )
    {
        return -4;
    }
    return 0;
}

gdsconf_server_instance*
create_server_instance( const gdsconf_confinfo_t* conf_infos,
                        const int                 num_infos )
{
    if ( num_infos <= 0 )
    {
        fprintf( stderr,
                 "Must start server with at least one gdsconf_confinfo_t "
                 "structure.\n" );
    }
    // allocate server structure
    gdsconf_server_instance* new_server =
        (gdsconf_server_instance*)malloc( sizeof( gdsconf_server_instance ) );
    if ( new_server == NULL )
    {
        return NULL;
    }
    memset( new_server, 0, sizeof( gdsconf_server_instance ) );

    // allocate for the confinfo structs
    new_server->conf_info =
        (gdsconf_confinfo_t*)calloc( num_infos, sizeof( gdsconf_confinfo_t ) );
    if ( new_server->conf_info == NULL )
    {
        destroy_server_instance( new_server );
        return NULL;
    }
    memcpy( new_server->conf_info,
            conf_infos,
            sizeof( gdsconf_confinfo_t ) * num_infos );
    new_server->num_conf_infos = num_infos;

    // populate server strings
    new_server->service_strings = (char**)calloc( num_infos, sizeof( char* ) );
    if ( NULL == new_server->service_strings )
    {
        destroy_server_instance( new_server );
        return NULL;
    }
    memset( new_server->service_strings, 0, sizeof( char* ) * num_infos );
    for ( int i = 0; i < num_infos; ++i )
    {
        new_server->service_strings[ i ] =
            conf_info_to_string( &conf_infos[ i ] );
        if ( NULL == new_server->service_strings[ i ] )
        {
            destroy_server_instance( new_server );
            return NULL;
        }
    }

    /* This may look a bit ugly.  The answer should be a string of the form
    * "awg a b hostname prognum progver...." where 'a' is a unique value
    * which is 0 <= a < TP_MAX_NODE (128 in advanced ligo).
    * Attempt to get this value to use as a delay to keep all of the
    * servers from responding at the same time.
    */
    int delay = conf_infos[ 0 ].node;

    if ( !( delay >= 0 && delay < 128 ) )
    {
        /* Assign some other number to delay */
        delay = ( getpid( ) % 100 ) + 128;
        printf( "Delay based on PID = %d\n", delay );
    }
    else
    {
        delay *= 2;
        delay += 10;
        printf( "Delay based on DCUID * 2 + 10 = %d\n", delay );
    }

    new_server->delay_time.tv_sec = 0;
    new_server->delay_time.tv_nsec = delay * 1000000;

    int err_no = open_socket( new_server );
    if ( err_no != 0 )
    {
        fprintf( stderr, "Error opening conf server socket: %d\n", err_no );
        destroy_server_instance( new_server );
        return NULL;
    }

    // start server thread
    err_no = pthread_create(
        &new_server->thread, NULL, conf_server_thread_loop, new_server );
    if ( err_no )
    {
        fprintf( stderr, "Error spawning conf server thread: %d\n", err_no );
        destroy_server_instance( new_server );
        return NULL;
    }
    return new_server;
}

void
destroy_server_instance( gdsconf_server_instance* conf_server )
{
    if ( NULL == conf_server )
    {
        return;
    }
    conf_server->closing = 1;
    if ( conf_server->thread != 0 )
    {
        int err_no = pthread_join( conf_server->thread, NULL );
        if ( err_no )
        {
            fprintf( stderr, "Error joining conf server thread: %d\n", err_no );
        }
    }

    if ( conf_server->service_strings != NULL )
    {
        for ( int i = 0; i < conf_server->num_conf_infos; ++i )
        {
            if ( NULL != conf_server->service_strings[ i ] )
            {
                free( conf_server->service_strings[ i ] );
            }
        }
        free( conf_server->service_strings );
    }

    if ( conf_server->conf_info != NULL )
    {
        free( conf_server->conf_info );
    }
    free( conf_server );
}

/*----------------------------------------------------------------------*/
/*                                                         		*/
/* External Procedure Name: gdsconf_server_start					*/
/*                                                         		*/
/* Procedure Description: starts a configuration server task		*/
/*                                                         		*/
/* Procedure Arguments: list of services, # of services     	*/
/*                                                         		*/
/* Procedure Returns: A server handle if successful, or NULL		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/
GDSCONF_SERVER_HANDLE
gdsconf_server_start( const gdsconf_confinfo_t confs[], int num )
{
    /* initialize services */
    struct gdsconf_server_instance* new_server =
        create_server_instance( confs, num );

    return (GDSCONF_SERVER_HANDLE)new_server;
}

void
gdsconf_server_stop( GDSCONF_SERVER_HANDLE conf_server )
{
    destroy_server_instance( conf_server );
}

void
gdsconf_confinfo_setup( gdsconf_confinfo_t* conf_info,
                        const char*         interface,
                        int                 node,
                        int                 prognum,
                        int                 progver )
{
    strncpy(
        conf_info->interface, interface, sizeof( conf_info->interface ) - 1 );
    conf_info->node = node;
    conf_info->port_prognum = prognum;
    conf_info->progver = progver;

    conf_info->num = 0;

    struct in_addr host;
    memset( &host, 0, sizeof( host ) );

    struct ifaddrs *addrs, *addrs_start;
    if ( getifaddrs( &addrs_start ) )
    {
        fprintf( stderr,
                 "gdsconf_confinfo_setup(): Failed to get host address.\n"
                 "\tProceeding with a default address.\n"
                 "\tThis won't work for some older clients.\n" );
    }
    else
    {
        addrs = addrs_start;
        while ( addrs != NULL )
        {
            if ( addrs->ifa_addr->sa_family == AF_INET )
            {
                struct sockaddr_in* in_add =
                    (struct sockaddr_in*)addrs->ifa_addr;
                //printf("%s %s\n", addrs->ifa_name, inet_ntoa(in_add->sin_addr));
                // just take first non-loopback address
                if ( strcmp( addrs->ifa_name, "lo" ) != 0 )
                {
                    memcpy( &host, &in_add->sin_addr, sizeof( host ) );
                    break;
                }
            }
            else
            {
                //printf("%s not an IP4 address: Family %d\n", addrs->ifa_name);
            }
            addrs = addrs->ifa_next;
        }
        freeifaddrs( addrs_start );
    }
    strncpy(
        conf_info->host, inet_ntoa( host ), sizeof( conf_info->host ) - 1 );
}