//
// Created by erik.vonreis on 5/4/23.
//

#ifndef CDS_CRTOOLS_RPCDEFS_H
#define CDS_CRTOOLS_RPCDEFS_H

/** Default timeout for the rpcProbe function (in sec).

@author DS, August 98
                @see Remote Procedure Call Interface
                    ************************************************************************/
#define RPC_PROBE_WAIT 1

/** Default program number for the test point server. The default
    program number is 0x31001001.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_TESTPOINT 0x31002000

/** Default version number for the test point server. The default
    version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_TESTPOINT 1

/** Default program number for the arbitrary waveform generator. The
    default program number is 0x31001002.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_AWG 0x31003000

/** Default version number for the arbitrary waveform generator. The
    default version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_AWG 1

/** Default program number for the diagnostics data server. The default
    program number is 0x31001003.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_RTDD 0x31001003

/** Default version number for the diagnostics data server. The default
    version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_RTDD 1

/** Default program number for the diagnostics message server. The
    default program number is 0x31001004.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_GDSMSG 0x31001004

/** Default version number for the diagnostics message server. The
    default version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_GDSMSG 1

/** Default program number for the diagnostics channel database server.
    The default program number is 0x31001005.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_GDSCHN 0x31001005

/** Default version number for the diagnostics channel database server.
    The default version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_GDSCHN 1

/** Default program number for the leap second information server.
    The default program number is 0x31001006.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_GDSLEAP 0x31001006

/** Default version number for the leap second information server.
    The default version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_GDSLEAP 1

/** Default program number for the launch server.
    The default program number is 0x31001007.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGNUM_LAUNCH 0x31001007

/** Default version number for the launch server.
    The default version number is 1.

    @author DS, August 98
    @see Remote Procedure Call Interface
************************************************************************/
#define RPC_PROGVER_LAUNCH 1

#endif // CDS_CRTOOLS_RPCDEFS_H
