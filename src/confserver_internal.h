#ifndef CONFSERVER_INTERNAL_H
#define CONFSERVER_INTERNAL_H

#include <time.h>
#include "gdsconf/gdsconftype.h"

typedef struct gdsconf_server_instance
{
    gdsconf_confinfo_t* conf_info;
    int                 num_conf_infos;
    pthread_t           thread;
    int                 closing;
    int                 sock;
    char**              service_strings;
    struct timespec     delay_time;
} gdsconf_server_instance;

/**
 * Create a server instance from a set of conf info structs
 *
 * Also creates the server thread that handles configuraiton requests.
 *
 * @returns@ NULL on an error.
 */
gdsconf_server_instance*
create_server_instance( const gdsconf_confinfo_t* conf_infos, int num_infos );

/**
 * Join the server thread, then free memory associated with the server.
 */
void destroy_server_instance( gdsconf_server_instance* );

/**
 * Convert conf_info structure to a gdsconf server string
 * @param conf_info
 * @return
 */
char* conf_info_to_string( const gdsconf_confinfo_t* conf_info );

#endif // CONFSERVER_INTERNAL_H