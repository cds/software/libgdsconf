//
// Created by erik.vonreis on 9/13/23.
//

#define MAJ_VER 0
#define MIN_VER 1
#define REL_VER 6

/**
 * Network interface versions
 *
 * 1 - broadcast plain string protocol inherited from ALIGO.
 *
 */
#define NETWORK_INTERFACE_VERSION 1

#include "stdlib.h"
#include "stdio.h"

#ifndef LIBGDSCONF_VERSION
#error "LIBGDSCONF_VERSION must be defined"
#endif

static int
version_compare( int maj0, int min0, int rel0, int maj1, int min1, int rel1 )
{
    if ( maj0 < maj1 )
    {
        return -1;
    }
    if ( maj0 > maj1 )
    {
        return 1;
    }
    if ( min0 < min1 )
    {
        return -1;
    }
    if ( min0 > min1 )
    {
        return 1;
    }
    if ( rel0 < rel1 )
    {
        return -1;
    }
    if ( rel0 > rel1 )
    {
        return 1;
    }
    return 0;
}

void
gdsconf_version( int* major_version, int* minor_version, int* release_version )
{
    sscanf( LIBGDSCONF_VERSION,
            "%d.%d.%d",
            major_version,
            minor_version,
            release_version );
}

const char*
gdsconf_is_compatible_with( int major_version,
                            int minor_version,
                            int release_version )
{
    int maj_ver, min_ver, rel_ver;

    gdsconf_version( &maj_ver, &min_ver, &rel_ver );

    int comp = version_compare( major_version,
                                minor_version,
                                release_version,
                                maj_ver,
                                min_ver,
                                rel_ver );
    if ( comp <= 0 )
    {
        return NULL;
    }
    return "Requested version is newer than the library version.";
}

int
gdsconf_network_interface_version( )
{
    return NETWORK_INTERFACE_VERSION;
}