//
// Created by erik.vonreis on 9/11/23.
//

#ifndef LIBGDSCONF_CONFTYPE_INTERNAL_H
#define LIBGDSCONF_CONFTYPE_INTERNAL_H

#include "gdsconf/gdsconftype.h"
char* conf_info_to_string( const gdsconf_confinfo_t* conf_info );

#endif //LIBGDSCONF_CONFTYPE_INTERNAL_H
