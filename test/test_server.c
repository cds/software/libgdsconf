//
// Created by erik.vonreis on 8/7/23.
//
#include "gdsconf/gdsconfserver.h"
#include "time.h"
#include "unistd.h"

int
main( int argc, char* argv[] )
{
    gdsconf_confinfo_t confinfo[] = {
        { CONFIG_SERVICE_AWG, 1, 0, "localhost", 1, 1 },
        { CONFIG_SERVICE_TP, 1, 0, "localhost", 1, 1 }
    };
    gdsconf_confinfo_setup( confinfo, CONFIG_SERVICE_AWG, 1, 2, 3 );
    gdsconf_confinfo_setup( confinfo + 1, CONFIG_SERVICE_TP, 1, 2, 3 );

    GDSCONF_SERVER_HANDLE server = gdsconf_server_start( confinfo, 2 );
    usleep( 10000000 );
    gdsconf_server_stop( server );
}