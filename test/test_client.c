//
// Created by erik.vonreis on 9/8/23.
//
#include <unistd.h>
#include <stdio.h>

#include "gdsconf/gdsconfclient.h"

int
main( int argc, char* argv[] )
{
    if ( !gdsconf_client_supports_network_interface_version( 1 ) )
    {
        fprintf( stderr, "does not support earliest network interface" );
        return 1;
    }

    printf( "network interface = %d\n", gdsconf_network_interface_version( ) );

    int majv, minv, relv;

    gdsconf_version( &majv, &minv, &relv );
    printf( "Version = %d.%d.%d\n", majv, minv, relv );

    if ( NULL != gdsconf_is_compatible_with( majv, minv, relv ) )
    {
        fprintf( stderr,
                 "library reports it's not compatible with its own version\n" );
        return 1;
    }

    if ( NULL != gdsconf_is_compatible_with( 0, 0, 0 ) )
    {
        fprintf( stderr,
                 "library reports it's not compatible with the earliest "
                 "compatible version\n" );
        return 1;
    }

    if ( NULL == gdsconf_is_compatible_with( majv, minv, relv + 1 ) )
    {
        fprintf( stderr,
                 "library reports it's compatible with a newer version.\n" );
        return 1;
    }

    GDSCONF_HANDLE confs = gdsconf_refresh_records( 5 );
    if ( NULL == confs )
    {
        fprintf( stderr, "couldn't get configurations\n" );
        return 1;
    }
    GDSCONF_HANDLE confs2 = gdsconf_get_records( );
    if ( confs2 != confs )
    {
        fprintf( stderr,
                 "couldn't retrieve second handle to configurations\n" );
        return 1;
    }
    gdsconf_free_handle( confs2 );
    gdsconf_record_t* records;
    size_t            numrecs;
    char              outbuf[ 256 ];
    if ( gdsconf_get_confinfo( confs, &records, &numrecs ) )
    {
        for ( int i = 0; i < numrecs; ++i )
        {
            if ( gdsconf_record_to_string(
                     records + i, outbuf, sizeof( outbuf ) ) )
            {
                printf( "%s\n", outbuf );
            }
            else
            {
                fprintf( stderr, "could not convert diag service to string\n" );
                return 1;
            }
        }
    }
    gdsconf_free_handle( confs );
    GDSCONF_HANDLE new_confs = gdsconf_refresh_records( 1 );
    gdsconf_refresh_records( 1 );
    gdsconf_free_handle( new_confs );
}